<?php /*
@extends('partials.header')
@section('title', 'EME Propiedad Raiz - Ventas y Arriendos en Medellín, Colombia.')
@section('content')*/
?><div id="page-content_home">
			<div class="home_2">
				<div id="owl-demo-header" class="owl-carousel owl-theme carousel-full-width">
					<div class="item">
						<span class="filter"></span>
						<div class="head-title-2" style="background:url( {{  URL::asset('/img/001.jpg') }} ) center;background-size: cover;">
							<div class="container">
								<div class="title col-lg-6 col-lg-offset-0 col-md-6 col-sm-8 col-sm-offset-1 col-xs-9 col-xs-offset-1">
									<h1>La forma más <br> facil de encontrar <br> tu inmueble</h1>
									<span class="ffs-bs"><a href="property_page.html" class="btn btn-large btn-primary">Saber más</a></span>
								</div>
							</div>
						</div>
					</div>
                    
					
                    <div class="item">
						<span class="filter"></span>
						<div class="head-title-2" style="background:url({{ asset('/img/001.jpg') }}) center;background-size: cover;">
							<div class="container">
								<div class="title col-lg-6 col-lg-offset-0 col-md-6 col-sm-8 col-sm-offset-1 col-xs-9 col-xs-offset-1">
									<h1>La forma más <br> facíl de encontrar <br> tu inmueble</h1>
									<span class="ffs-bs"><a href="property_page.html" class="btn btn-large btn-primary">Saber más</a></span>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="container">
				<div class="explore">
					<h2 class="light">¿Qué estas <span class="italic">buscando</span>?</h2>
					<h5><i class="fa fa-map-marker"></i><span class="team-color">Las mejores propiedades de</span> Medellín</h5>
				</div>
                
                                    <div class="h40"></div>

				<div class="row">
					<div class="col-md-4 col-sm-6">
                    	<div class="top_foto" style="background-color:#16c6e6"><img src="{{ asset('/img/006.png') }}" /> Proyectos</div>
						<a href="<?php /*echo base_url()*/?>proyectos" class="exp-img no-top" style="background:url({{ asset('/img/003.jpg') }}) center;background-size: cover;">
							<span class="filter"></span>
							<div class="img-info">
								<span class="ffs-bs btn btn-small btn-primary">Ver todos</span>
							</div>
						</a>
					</div>
                    
					<div class="col-md-4 col-sm-6">
                    	<div class="top_foto" style="background-color:#97cb31"><img src="{{ asset('/img/007.png') }}" /> Usados</div>
						<a href="<?php /*echo base_url()*/?>usados" class="exp-img no-top" style="background:url({{ asset('/img/004.jpg') }}) center;background-size: cover;">
							<span class="filter"></span>
							<div class="img-info">
								<span class="ffs-bs btn btn-small btn-primary">Ver todos</span>
							</div>
						</a>
					</div>


					<div class="col-md-4 col-sm-6">
                    	<div class="top_foto" style="background-color:#f7d32c"><img src="{{ asset('/img/008.png') }}" /> Arrendamientos</div>
						<a href="<?php /*echo base_url()*/?>arrendamientos" class="exp-img no-top" style="background:url({{ asset('/img/005.jpg') }}) center;background-size: cover;">
							<span class="filter"></span>
							<div class="img-info">
								<span class="ffs-bs btn btn-small btn-primary">Ver todos</span>
							</div>
						</a>
					</div>



				</div>
			</div>