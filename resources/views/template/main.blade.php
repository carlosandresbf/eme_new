<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
   
	<link href="{{ asset('fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="{{ asset('css/jquery.slider.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('>assets/css/owl.carousel.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">

              <script src="{{ asset('js/jquery-1.10.2.min.js') }}"></script>
  
  <script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
              
    <script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.placeholder.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('js/retina-1.1.0.min.js') }}"></script>
	
	<script type="text/javascript" src="{{ asset('js/selectize.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/tmpl.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/icheck.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.dependClass-0.1.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/draggable-0.1.js') }}"></script>
	   <?php /*?> <script type="text/javascript" src="{{ asset('js/markerwithlabel_packed.js"></script><?php */?>
	<script type="text/javascript" src="{{ asset('js/jquery.slider.js') }}"></script>

	<script type="text/javascript" src="{{ asset('js/custom-map.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.fallings.js') }}"></script>
	<title>{{ yield('title') }}  </title>
</head>
<body class="page-homepage" id="page-top">
	<div id="page-preloader">
		<div class="loader-ring"></div>
		<div class="loader-ring2"></div>
	</div>
    
{{ yield('header') }}    
    
{{ yield('content') }}   
		
		<!-- end Page Content -->
		
		<!-- Start Footer -->
  <div id="footer">


	<meta charset="UTF-8">
	<title>Footer</title>


	<footer id="page-footer">
		<div class="inner">
			<!-- /#footer-main -->
			<section id="footer-thumbnails" class="footer-thumbnails"></section><!-- /#footer-thumbnails -->
			<section id="footer-copyright">
				<div class="container">
					© EME Propiedad Raíz | Todos los derechos reservados 2016

					<span class="pull-right"><a href="http://imganinamos.com">Desarrollo web : imaginamos.com</a></span>
				</div>
			</section>
		</div><!-- /.inner -->
	</footer>



</div>
    <!-- End Footer -->
	</div>

	<!-- Modal login, register, custom gallery -->
	<div id="login-modal-open"></div>
	<div id="register-modal-open"></div>
    
    
	
	<!-- End Modal login, register, custom gallery -->


    <script>
		jQuery( document ).ready(function() {
	
		$(".main-title").fallings({
			velocity: 3
		});
	
		$(".primary-image").fallings({
			velocity: .5,
			bgParallax: true,
			bgPercent: '50%'
		});
	
	});
	</script>
	<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/custom-map.js') }}"></script>
<!--[if gt IE 8]>
<script type="text/javascript" src="{{ asset('js/ie.js"></script>
<![endif]-->
</body>
</html>